#include "RenderPrimitives.h"

using namespace CNE::Graphics::Rendering;

void PrimitiveRenderer::Clear()
{
	
}

void PrimitiveRenderer::Draw(const VertexArray& va, const IndexBuffer& ib, const Material& mat) const
{
	mat.Use();
	va.Bind();
	ib.Bind();

	glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, 0);
}
