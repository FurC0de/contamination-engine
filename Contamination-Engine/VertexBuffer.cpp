#include "VertexBuffer.h"
#include <glew.h>
#include <glfw3.h>

CNE::Graphics::Primitives::VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
	//void* a = &data;
	glGenBuffers(1, &cne_RendererID);
	glBindBuffer(GL_ARRAY_BUFFER, cne_RendererID);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

CNE::Graphics::Primitives::VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &cne_RendererID);
}

void CNE::Graphics::Primitives::VertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, cne_RendererID);
}

void CNE::Graphics::Primitives::VertexBuffer::Unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
