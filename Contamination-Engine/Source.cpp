
#include <iostream>
#include <array>

#include "Shaders/shader.h"
#include "Texture/textureManager.h"
#include "Materials/Material.h"
#include "RenderPrimitives.h"

#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"

#include "IL/il.h"
#include "IL/ilu.h"
#include "IL/ilut.h"

#include "External/dimgui/imgui.h"
#include "External/dimgui/imgui_impl_glfw.h"
#include "External/dimgui/imgui_impl_opengl3.h"

#define GLEW_STATIC
#include <glew.h>
#include <glfw3.h>

#include "External/glm/glm.hpp"
#include "External/glm/gtc/matrix_transform.hpp"
#include "External/glm/gtc/type_ptr.hpp"

using namespace CNE::Graphics::Primitives;
using namespace CNE::Graphics::Shaders;
using namespace CNE::Graphics::Textures;
using namespace CNE::Graphics::Materials;
using namespace CNE::Graphics::Rendering;

// Window dimensions
static GLFWwindow* editorWindow;
const GLuint WIDTH = 800, HEIGHT = 600;

static TextureManager* EngineTextureManager;
static PrimitiveRenderer* EngineRenderer;


void draw() {
}

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

static void APIENTRY glDebugCallB(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	std::string msgSource;
	switch (source) {
	case GL_DEBUG_SOURCE_API:
		msgSource = "WINDOW_SYSTEM";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		msgSource = "SHADER_COMPILER";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		msgSource = "THIRD_PARTY";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		msgSource = "APPLICATION";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		msgSource = "OTHER";
		break;
	}

	std::string msgType;
	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		msgType = "ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		msgType = "DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		msgType = "UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		msgType = "PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		msgType = "PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_OTHER:
		msgType = "OTHER";
		break;
	}

	std::string msgSeverity;
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:
		msgSeverity = "LOW";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		msgSeverity = "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_HIGH:
		msgSeverity = "HIGH";
		break;
	}

	printf("glDebugMessage:\n%s \n type = %s source = %s severity = %s\n", message, msgType.c_str(), msgSource.c_str(), msgSeverity.c_str());
}

void GL_set_window_parameters() {
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DECORATED, false);
}

void GL_initialize() {
	
	glfwSetErrorCallback(error_callback);
	glfwInit();
	GL_set_window_parameters();
	editorWindow = glfwCreateWindow(WIDTH, HEIGHT, "Opia Engine", nullptr, nullptr);
	glfwMakeContextCurrent(editorWindow);
	
	glewExperimental = GL_TRUE;
	glewInit();

	// Set debug callback
	if (glDebugMessageCallback != NULL) {
		glDebugMessageCallback(glDebugCallB, NULL);
	}
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEPTH_TEST);

	ilInit();
	iluInit();

	glViewport(0, 0, WIDTH, HEIGHT);
	

	EngineTextureManager = new TextureManager();
	EngineRenderer = new PrimitiveRenderer();

	std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

}


int main()
{
	
	GL_initialize();
	
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	ImGuiStyle& style = ImGui::GetStyle();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}

	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(editorWindow, true);
	const char* glsl_version = "#version 430";
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	ImFont* font = io.Fonts->AddFontFromFileTTF("TitilliumWeb.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesDefault());
	//IM_ASSERT(font != NULL);
	
	Shader shader("Shaders/test.vxs", "Shaders/test.fgs"); // Build and compile our test shader program

	std::vector<std::string> sources = { "Resources/large_forerunner_floor_plate.jpg",  "Resources/large_forerunner_floor_plate.jpg",  "Resources/large_forerunner_floor_plate.jpg",  "Resources/large_forerunner_floor_plate.jpg" };	// Manually define the textures we need
	EngineTextureManager->LoadTextures(&sources);											// Load textures, stored in "sources" with our default texture manager

	Material mat(EngineTextureManager, (unsigned int)3128822818, &shader );

	float positions[] = {
		-0.5f, -0.5f,	0.0f, 0.0f,
		 0.5f, -0.5f,	1.0f, 0.0f,
		 0.5f,  0.5f,	1.0f, 1.0f,
		-0.5f,  0.5f,	0.0f, 1.0f
	};

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};
	
	
	VertexArray va;
	VertexBuffer vb(positions, 4 * 4 * sizeof(float));

	VertexBufferLayout layout;	
	layout.Push<float>(2);
	layout.Push<float>(2);
	va.AddBuffer(vb, layout);

	IndexBuffer ib(indices, 6);

	va.Unbind(); vb.Unbind(); ib.Unbind(); shader.Unbind();

	//ImGui::CreateContext();
	//ImGui_ImplGlfw_InitForOpenGL(editorWindow, true);
	//const char* glsl_version = "#version 430";
	//ImGui_ImplOpenGL3_Init(glsl_version);
	//ImGui::StyleColorsDark;
	
	while (!glfwWindowShouldClose(editorWindow))
	{
		// Get secondary events (mouse, key press...)
		glfwPollEvents();

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		/* Draw something */

		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Edit"))
			{
				if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
				if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
				ImGui::Separator();
				if (ImGui::MenuItem("Cut", "CTRL+X")) {}
				if (ImGui::MenuItem("Copy", "CTRL+C")) {}
				if (ImGui::MenuItem("Paste", "CTRL+V")) {}
				ImGui::EndMenu();
			}

			ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.0f);

			const float ItemSpacing = ImGui::GetStyle().ItemSpacing.x;

			ImGui::SameLine(ImGui::GetWindowWidth() - 35);
			if (ImGui::Button(" x ")) {}

			ImGui::SameLine(ImGui::GetWindowWidth() - 60);
			if (ImGui::Button(" _ ")) {}

			ImGui::PopStyleVar();
			ImGui::EndMainMenuBar();
		}

		{
			ImGui::Begin("Active Textures");                          // Create a window called "Hello, world!" and append into it.
			
			
			unsigned int _a = EngineTextureManager->TexturesActive();
			static std::vector<bool> selected(_a);
			unsigned int x = 0, y = 0;

			for (list<TextureImage>::iterator it = std::find_if(EngineTextureManager->Textures.begin(), EngineTextureManager->Textures.end(), [](TextureImage texim) { return texim.spaceState == CNE::CNE_DYNAMIC_USED; });
				it != EngineTextureManager->Textures.end();
				it = std::find_if(++it, EngineTextureManager->Textures.end(), [](TextureImage texim) { return texim.spaceState == CNE::CNE_DYNAMIC_USED; }))
			{
				
				ImVec2 alignment = ImVec2((float)x / 2.0f, (float)y / 2.0f);
				ImVec2 size = ImVec2((ImGui::GetWindowWidth() - ImGui::GetStyle().ItemSpacing.x * 3) / 3, (ImGui::GetWindowWidth() - ImGui::GetStyle().ItemSpacing.x * 3) / 3);
				//ImGui::PushStyleVar(ImGuiStyleVar_SelectableTextAlign, alignment);
				char name[64];

				sprintf_s(name, "  %s \n hello", (&*it)->name.c_str()); //[%X] //, (unsigned int)(&*it)->hash
				if (x > 0) ImGui::SameLine();
				
				ImVec2 combo_pos = ImGui::GetCursorScreenPos();

				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.8f);
				ImGui::Image((void*)(intptr_t)(&*it)->texID, size);
				ImGui::PopStyleVar();

				ImGui::SameLine();
				ImGui::SetCursorScreenPos(combo_pos);
				
				ImGui::PushStyleColor(ImGuiCol_HeaderHovered, ImVec4(0.6f, 0.6f, 0.6f, 0.5f));
				ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.0f));
				
				ImGui::Selectable(name, selected[3 * y + x], ImGuiSelectableFlags_None, size);

				//ImGui::PopStyleVar(1);
				ImGui::PopStyleColor(2);

				x++;
				if (x > 2) {
					x = 0;
					y++;
				}

			}

			//if (ImGui::TreeNode("Texture browser"))
			//{
			//	ImGui::TreePop();
			//}

			ImGui::End();
		}

		{
			static float f = 0.0f;
			static int counter = 0;

			ImGui::Begin("OpenGL statistics");                          // Create a window called "Hello, world!" and append into it.

			ImGui::Text("Object`s transparency: ");               // Display some text (you can use a format strings too)

			ImGui::SliderFloat("%", &f, 0.0f, 100.0f);            // Edit 1 float using a slider from 0.0f to 1.0f

			glfwSetWindowPos(editorWindow, f*3, f);

			//ImGui::ColorEdit3("clear color", (float*)& clear_color); // Edit 3 floats representing a color

			if (ImGui::Button("Reinit OpenGL"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
				counter++;
			ImGui::SameLine();
			ImGui::Text("OpenGL reinitialized %d times", counter);

			ImGui::Text("Render contex average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::End();

		}

		// Rendering
		ImGui::Render();
		int display_w, display_h;
		glfwGetFramebufferSize(editorWindow, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		

		glClearColor(0.09f, 0.09f, 0.09f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		EngineRenderer->Draw(va, ib, mat);

		glfwSwapBuffers(editorWindow);
		// Update and Render additional Platform Windows
		// (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
		//  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			GLFWwindow* backup_current_context = glfwGetCurrentContext();
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
			glfwMakeContextCurrent(backup_current_context);
		}

	}

	glfwTerminate();
	return 0;

}