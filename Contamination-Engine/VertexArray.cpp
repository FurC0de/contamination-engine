#include <glew.h>
#include <glfw3.h>
#include <iostream>
#include "VertexArray.h"

CNE::Graphics::Primitives::VertexArray::VertexArray()
{
	glGenVertexArrays(1, &cne_RendererID);
}

CNE::Graphics::Primitives::VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &cne_RendererID);
}

void CNE::Graphics::Primitives::VertexArray::AddBuffer(const VertexBuffer& vb, VertexBufferLayout& layout)
{
	Bind();
	vb.Bind();
	const auto& elements = layout.GetElements();

	unsigned int offset = 0;

	for (int i = 0; i < elements.size(); i++) {
		const auto& element = elements[i];
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, element.count, element.type, element.isNormalized, layout.GetStride(), (const void*)offset);
		std::cout << i << ", " << element.count << ", " << (int)element.type << ", " << (int)element.isNormalized << ", " << layout.GetStride() << ", " << (const void*)offset << std::endl;
		offset += VertexBufferElement::getElementSize(element.type);
	}

}

void CNE::Graphics::Primitives::VertexArray::Bind() const
{
	glBindVertexArray(cne_RendererID);
}

void CNE::Graphics::Primitives::VertexArray::Unbind() const
{
	glBindVertexArray(0);
}
