#ifndef CORE_H
#define CORE_H

#include <string>
#include <sstream>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <locale>
#include <codecvt>
#include <tuple>

#include "Utilities/Enum.h"

namespace CNE {
	
	char PRESENT_PRIORITY;
	char PRESENT_INFO_BORDER;

	namespace Core {

		typedef struct UserFriendlyParameters {
			std::string name;
		};
		
		/* !!! NECESSARY TO MOVE THESE STRUCTURES FROM CORE TO A STORAGE !!! */
		/// Struct used for passing to storage manager. Used for loading textures, models, etc.
		typedef struct sLoadRequest
		{
			char storage_type = CNE_STORAGE_REAL;	/// From where we want to load a texture (CNE_STORAGE_VIRTUAL, CNE_STORAGE_NAMED, CNE_STORAGE_REAL)
			std::string filename;							/// Link to a file if storage type is CNE_STORAGE_REAL
			int link;										/// Link to a texture if storage type is CNE_STORAGE_VIRTUAL or CNE_STORAGE_NAMED. Encoded by a 4 bytes (int)

		} sLoadRequest;

		//char* ReadResource(sLoadRequest request); /// Reads single file (resource) from different types of CNE_STORAGE



		std::ostream& TerminalOut(CNE_INFO_TYPE type);

	}
}

#endif