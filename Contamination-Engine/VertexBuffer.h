#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

namespace CNE {
	namespace Graphics {
		namespace Primitives {
			class VertexBuffer {
			private:
				unsigned int cne_RendererID;

			public:
				VertexBuffer(const void* data, unsigned int size);
				~VertexBuffer();

				void Bind() const;
				void Unbind() const;
			};
		}
	}
}

#endif // !VERTEXBUFFER_H
