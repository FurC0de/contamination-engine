#ifndef _TEXTURE_H
#define _TEXTURE_H

#include "IL/il.h"
#include "IL/ilu.h"
#include <glew.h>
#include <string>

#include "../Utilities/Enum.h"

using namespace std;
//using namespace CNE;

namespace CNE {
	namespace Graphics {
		namespace Textures {

			enum CNE_TEXTURE_TYPE {
				CNE_TEXTURE_DIFFUSE,
				CNE_TEXTURE_ALBEDO,
				CNE_TEXTURE_AMBIENT_OCCLUSION,
				CNE_TEXTURE_NORMAL,
				CNE_TEXTURE_DISPLACEMENT,
				CNE_TEXTURE_REFLECTION,
				CNE_TEXTURE_GLOSS
			};

			typedef struct TextureImage
			{
				GLubyte* imageData;		/// Image data (array)
				GLuint bpp;				/// Byte per pixel
				GLuint width, height;	/// Size
				GLuint texID;			/// Real OGL ID of texture
				std::string name;		/// User-friendly texture name

				unsigned int hash;		/// Texture`s hash

				char unloadingState = CNE_UNREQUESTED;	/// CNE_REQUESTED when we need to unload texture from RAM but leave all indexes for future
				char spaceState = CNE_DYNAMIC_FREE;		/// CNE_DYNAMIC_FREE is default on creation. If CNE_DYNAMIC_RESERVED then LoadTexture will use texID as if it is defined
			};

			typedef struct ModelTexture {
				TextureImage* image;	/// Pointer to a TextureImage, so different objects could use a texture more than once, passing to a different types of a texture shader (Diffuse, albedo, AO, etc.)
				char type;				/// Type of a texture, CNE_TEXTURE_TYPE used
			};


			typedef struct ModelTextureCollection {
				TextureImage* Diffuse;
				TextureImage* Albedo;
				TextureImage* AmbientOcclusion;
				TextureImage* Normal;
				TextureImage* Displacement;
				TextureImage* Reflection;
				TextureImage* Gloss;
			};

			/**
			*	Class for basic Texture operations:
			*		Single: load, remove, unload, recover 
			*		General: getError
			**/
			class CTexture {
			public:

				CTexture();
				~CTexture();

				void LoadTexture(std::string filename, TextureImage* texture, ILenum FileType = IL_TYPE_UNKNOWN);
				void RemoveTexture(TextureImage* texture);
				void UnloadTexture(TextureImage* texture);
				void RecoverTexture(TextureImage* texture);
				void AssignHash(TextureImage* texture);

				static ModelTextureCollection CreateMTCollection(ModelTexture* mt[7]);
				static ModelTextureCollection CreateMTCollection(TextureImage* ti[7]);

			private:
				void _ProcessErrors(int _err, std::string _fname);
			};
		}
	}
}
#endif