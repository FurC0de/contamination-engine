#ifndef _TEXTURE_MANAGER_H
#define _TEXTURE_MANAGER_H

#include "IL/il.h"
#include "IL/ilu.h"

#include <string>
#include <vector>	
#include <list>
#include <unordered_map>

#include "../Utilities/Enum.h"
#include "texture.h"

namespace CNE {
	namespace Graphics {
		namespace Textures {

			class TextureManager {
			public:
				/// Constructors, destructors
				TextureManager();
				~TextureManager();

				bool ChangesFlag;

				void Allocate(int amount, bool reserved);
				void LoadTextures(std::vector<string>* src);
				void LoadTexture(std::string src);
				void RemoveTextures();
				void UnloadTextures();
				void RemoveTexture(TextureImage* texture);
				TextureImage* Find(unsigned int hash);
				ModelTexture* ReverseFind(unsigned int hash);
				ModelTexture* ReverseFind(const vector<unsigned int>& hash);

				/// Getters:
				unsigned int TexturesLoaded() const;	/// Total textures amount
				unsigned int TexturesActive() const;	/// Active textures amount
				unsigned int Allocated() const;		/// Total allocated data
				
				/// Public variables:
				std::list<TextureImage> Textures;

			private:
				GLuint _last_allocation;
				unsigned int _texturesLoaded;
				unsigned int _texturesActive;
				unsigned int _allocated;

				std::unordered_map<int, TextureImage*> _hashMap;
				
				CTexture* _basic;

			};
		}
	}
}
#endif