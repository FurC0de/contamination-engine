#include <iostream>
#include <string>

#include "texture.h"
#include "../External/MurmurHash2.h"

using namespace CNE::Graphics::Textures;
using namespace CNE::Utilities;

CTexture::CTexture()
{
	// � ������������ �������������� ���������� IL � ILU.
	ilInit();
	iluInit();
}

CTexture::~CTexture()
{	
}

void CTexture::_ProcessErrors(int _err, std::string _fname) {
	if (_err != IL_NO_ERROR)
	{
		/*
			IL_ILLEGAL_OPERATION - There is currently no image bound. Use ilGenImages and ilBindImage before calling this function.
			IL_COULD_NOT_OPEN_FILE - The file pointed to by FileName could not be opened. Either the file does not exist or is in use by another process.
			IL_INVALID_ENUM - Type was of an invalid value.
			IL_INVALID_PARAM - FileName, File or Lump was NULL.
			IL_INVALID_FILE_HEADER - The file had an invalid header and could not be loaded.
			IL_ILLEGAL_FILE_VALUE - The file could not be loaded due to an invalid value present.
			IL_OUT_OF_MEMORY - Could not allocate memory for the new image data.
			IL_LIB_JPEG_ERROR - Error occurred when trying to load a jpeg.
			IL_LIB_PNG_ERROR - Error occurred when trying to load a png.
		*/
		std::cout << _fname << ": ";
		switch (_err)
		{
		case IL_ILLEGAL_OPERATION:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_ILLEGAL_OPERATION (" << IL_ILLEGAL_OPERATION << ")" << std::endl; break;
		case IL_COULD_NOT_OPEN_FILE:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_COULD_NOT_OPEN_FILE (" << IL_COULD_NOT_OPEN_FILE << ")" << std::endl; break;
		case IL_INVALID_ENUM:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_INVALID_ENUM (" << IL_INVALID_ENUM << ")" << std::endl; break;
		case IL_INVALID_PARAM:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_INVALID_PARAM (" << IL_INVALID_PARAM << ")" << std::endl; break;
		case IL_INVALID_FILE_HEADER:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_INVALID_FILE_HEADER (" << IL_INVALID_FILE_HEADER << ")" << std::endl; break;
		case IL_ILLEGAL_FILE_VALUE:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_ILLEGAL_FILE_VALUE (" << IL_ILLEGAL_FILE_VALUE << ")" << std::endl; break;
		case IL_OUT_OF_MEMORY:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_OUT_OF_MEMORY (" << IL_OUT_OF_MEMORY << ")" << std::endl; break;
		case IL_LIB_JPEG_ERROR:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_LIB_JPEG_ERROR (" << IL_LIB_JPEG_ERROR << ")" << std::endl; break;
		case IL_LIB_PNG_ERROR:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::IL_LIB_PNG_ERROR (" << IL_LIB_PNG_ERROR << ")" << std::endl; break;
		default:
			std::cout << "ERROR::TEXTURE::NOT_LOADED::UNKNOWN (" << _err << ")" << std::endl; break;
		}

	}
	else {
		std::cout << "INTERNAL::TEXTURE::SOURCE::LOADING_OK" << std::endl;
	}
}


void CTexture::LoadTexture(std::string filename, TextureImage* texture, ILenum FileType) {

	if (texture->spaceState == CNE_DYNAMIC_USED) {
		return;
	}

	ilLoad(FileType, filename.c_str());
	_ProcessErrors(ilGetError(), filename);

	// Remove directory if present.
	// Do this before extension removal incase directory has a period character.
	const size_t last_slash_idx = filename.find_last_of("/");
	if (std::string::npos != last_slash_idx)
	{
		filename.erase(0, last_slash_idx + 1);
	}
	texture->name = filename;

	if (texture->spaceState == CNE_DYNAMIC_FREE) {
		glGenTextures(1, &texture->texID);
	}

	glBindTexture(GL_TEXTURE_2D, texture->texID); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	texture->width = ilGetInteger(IL_IMAGE_WIDTH); 
	texture->height = ilGetInteger(IL_IMAGE_HEIGHT);
	texture->imageData = ilGetData();

	ilEnable(IL_CONV_PAL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width, texture->height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->imageData);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Set space state to used. This will prevent texture from overwriting
	texture->spaceState = CNE_DYNAMIC_USED;

	// And assign a hash value of a texture, so we could find it in a stack later
	this->AssignHash(texture);

	// Unbinding texture operator
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CTexture::RemoveTexture(TextureImage* texture)
{
			
}

void CTexture::UnloadTexture(TextureImage* texture)
{
	if (texture->unloadingState != CNE_REQUEST_FINISHED) {
		glDeleteTextures(1, &(texture->texID));
		texture->imageData = 0;
		texture->unloadingState = CNE_REQUEST_FINISHED; // ���������� ���� ��� ���� ��������� ��� ���������
		texture->spaceState = CNE_DYNAMIC_RESERVED;		// ���������� ���� ��� ����� ��� ���� ��������� �������� ���������������
	}
}

void CTexture::RecoverTexture(TextureImage* texture)
{

}


void CTexture::AssignHash(TextureImage* texture) 
{
	texture->hash = MurmurHash2(texture->imageData, sizeof(texture->imageData), 0xFFFFFFFF);
	std::cout << "Texture(" << (int)texture << ") hash is "<<  texture->hash << std::endl;
}

ModelTextureCollection CNE::Graphics::Textures::CTexture::CreateMTCollection(ModelTexture* mt[7])
{
	ModelTextureCollection _mtc;

	for (int i = 0; i < 7; i++) {
		switch (i) {
		case CNE_TEXTURE_DIFFUSE:
			_mtc.Diffuse = mt[0]->image;
			break;
		case CNE_TEXTURE_ALBEDO:
			_mtc.Albedo = mt[1]->image;
			break;
		case CNE_TEXTURE_AMBIENT_OCCLUSION:
			_mtc.AmbientOcclusion = mt[2]->image;
			break;
		case CNE_TEXTURE_NORMAL:
			_mtc.Normal = mt[3]->image;
			break;
		case CNE_TEXTURE_DISPLACEMENT:
			_mtc.Displacement = mt[4]->image;
			break;
		case CNE_TEXTURE_REFLECTION:
			_mtc.Reflection = mt[5]->image;
			break;
		case CNE_TEXTURE_GLOSS:
			_mtc.Gloss = mt[6]->image;
			break;
		}
	}
	return _mtc;
}

ModelTextureCollection CNE::Graphics::Textures::CTexture::CreateMTCollection(TextureImage* ti[7])
{
	ModelTextureCollection _mtc;

	_mtc.Diffuse = ti[0];
	_mtc.Albedo = ti[1];
	_mtc.AmbientOcclusion = ti[2];
	_mtc.Normal = ti[3];
	_mtc.Displacement = ti[4];
	_mtc.Reflection = ti[5];
	_mtc.Gloss = ti[6];

	return _mtc;
}
