#include <iostream>
#include <string>
#include <algorithm>
#include "textureManager.h"

using namespace CNE::Graphics::Textures;

TextureManager::TextureManager() :
	_last_allocation(0), _texturesLoaded(0), _texturesActive(0), _allocated(0)
{
	ChangesFlag = false;
	_basic = new CTexture;
}

TextureManager::~TextureManager()
{
}

void TextureManager::Allocate(int amount, bool reserved)
{
	glGenTextures(amount, &_last_allocation);
	_allocated += amount;

	for (int i = 0; i < amount; i++) {
		TextureImage* N = new TextureImage;
		N->texID = _last_allocation + i;
		Textures.emplace_back(*N);
	}

}

void TextureManager::LoadTextures(std::vector<string>* src)
{
	///	Example:
	///	We need to load 20 textures, 10 are free (allocated 150, but 140 are used), so formula will be:
	///	20 - (150 - 140)
	///	(sources size) - (allocated - used)

	int _amount = src->size() - (_allocated - _texturesLoaded);

	if (_amount > 0) {
		Allocate(_amount, false);
	}

	int _counter = 0;
	for (list<TextureImage>::iterator it = std::find_if(Textures.begin(), Textures.end(), [](TextureImage texim) { return texim.spaceState == CNE_DYNAMIC_FREE; });
		it != Textures.end();
		it = std::find_if(++it, Textures.end(), [](TextureImage texim) { return texim.spaceState == CNE_DYNAMIC_FREE; }))
	{
		_basic->LoadTexture((*src)[_counter], &*it);
		_hashMap.insert({ (&*it)->hash, &*it });

		_counter++;
		if (_counter == _amount) {
			break;
		}
	}

	_texturesLoaded += src->size();
	_texturesActive += src->size();

}

void TextureManager::LoadTexture(std::string src)
{

}

void TextureManager::UnloadTextures()
{
	ChangesFlag = true;
}

void TextureManager::RemoveTexture(TextureImage* texture)
{
	ChangesFlag = true;

	if (texture->unloadingState == CNE_DYNAMIC_USED) {
		_basic->UnloadTexture(texture);
	}

	texture->unloadingState = CNE_REQUEST_INAPPLICABLE; // ���������� ���� ��� ��� ���������� ����������� �����-���� ������ ��������� � �������� � ����������
	texture->spaceState = CNE_DYNAMIC_FREE;				// ���������� ���� ��� ����� ��� ������ ��������� ������ ��������

}

TextureImage* TextureManager::Find(unsigned int hash)
{
	if (_hashMap.find(hash) != _hashMap.end()) {
		return _hashMap[hash];
	} else {
		return nullptr;
	}
}

ModelTexture* TextureManager::ReverseFind(const vector<unsigned int>& hash)
{
	return nullptr;
}

void TextureManager::RemoveTextures()
{
	ChangesFlag = true;
}

unsigned int TextureManager::TexturesLoaded() const
{
	return _texturesLoaded;
}
unsigned int TextureManager::TexturesActive() const
{
	return _texturesActive;
}
unsigned int TextureManager::Allocated() const
{
	return _allocated;
}