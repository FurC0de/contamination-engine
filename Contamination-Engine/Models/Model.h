#ifndef MODELS_H
#define MODELS_H

#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "ModelVertex.h"
#include "../Texture/texture.h"
#include "../Shaders/shader.h"
#include "../Materials/material.h"


using namespace CNE::Graphics::Textures;
using namespace CNE::Graphics::Shaders;
using namespace CNE::Graphics::Materials;

namespace CNE {
	namespace Graphics {
		namespace Models {

			class Mesh {
			public:
				/*  Mesh Data  */
				std::vector<ModelVertex> vertices;
				std::vector<unsigned int> indices;
				std::vector<ModelTexture> textures;
				Material* material;

				/*  Constructor  */
				Mesh(const vector<ModelVertex>& vertices, const vector<unsigned int>& indices, const vector<ModelTexture>& textures)
					: vertices(vertices)
					, indices(indices)
					, textures(textures)
				{
					setupMesh();
				}

				/*  Functions  */
				void Draw(Shader shader);

			private:
				/*  Render data  */
				unsigned int VAO, VBO, EBO;

				/*  Functions    */
				void setupMesh();
			};


			class Model
			{
			public:
				/*  Constructor  */
				Model(char* path)
				{
					loadModel(path);
				}

				void Draw(Shader shader);

			private:

				/*  ������ ������  */
				std::vector<Mesh> meshes;
				std::string directory;

				/*  ������   */
				void loadModel(string path);
				void processNode(aiNode* node, const aiScene* scene);
				Mesh processMesh(aiMesh* mesh, const aiScene* scene);
				vector<ModelTexture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);
			};
		}
	}
}
#endif