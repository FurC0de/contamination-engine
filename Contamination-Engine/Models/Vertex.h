#ifndef MODELVERTEX_H
#define MODELVERTEX_H

#include "../External/glm/glm.hpp"
#include "../External/glm/gtc/matrix_transform.hpp"
#include "../External/glm/gtc/type_ptr.hpp"

struct ModelVertex {
	glm::vec3 Position;		// fPos_x, fPos_y, fPos_z
	glm::vec3 Normal;		// fPos_x, fPos_y, fPos_z
	glm::vec2 TexCoords;	// fPos_s, fPos_t
};

#endif
