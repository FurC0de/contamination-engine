#include <glew.h>
#include <glfw3.h>
#include <sstream>

#include "Model.h"

#include "../VertexArray.h"
#include "../VertexBufferLayout.h"
#include "../IndexBuffer.h"

using namespace CNE::Graphics::Primitives;
using namespace CNE::Graphics::Models;

/* Mesh methods definitions */

void Mesh::Draw(Shader shader)
{
	// bind appropriate textures
	unsigned int diffuseN = 1;
	unsigned int specularN = 1;
	unsigned int normalN = 1;
	unsigned int heightN = 1;
	for (unsigned int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
		// retrieve texture number (the N in diffuse_textureN)
		string number;

		switch (textures[i].type)
		{
		case CNE_TEXTURE_DIFFUSE:

			break;
		case CNE_TEXTURE_ALBEDO:

			break;
		case CNE_TEXTURE_AMBIENT_OCCLUSION:

			break;
		case CNE_TEXTURE_NORMAL:

			break;
		case CNE_TEXTURE_DISPLACEMENT:

			break;
		case CNE_TEXTURE_REFLECTION:

			break;
		case CNE_TEXTURE_GLOSS:

			break;
		default:
			break;
		}

		/*
		if (type == "texture_diffuse")
			number = std::to_string(diffuseNr++);
		else if (name == "texture_specular")
			number = std::to_string(specularNr++); // transfer unsigned int to stream
		else if (name == "texture_normal")
			number = std::to_string(normalNr++); // transfer unsigned int to stream
		else if (name == "texture_height")
			number = std::to_string(heightNr++); // transfer unsigned int to stream*/


		// now set the sampler to the correct texture unit
		//glUniform1i(glGetUniformLocation(shader.ID, (name + number).c_str()), i);
		// and finally bind the texture
		//glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}

	// draw mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
}

void Mesh::setupMesh()
{
	IndexBuffer ib(&indices[0], indices.size());
	VertexArray va;
	VertexBuffer vb(&vertices[0], vertices.size() * sizeof(ModelVertex));


	VertexBufferLayout layout;
	layout.Push<float>(3); // vertex positions
	layout.Push<float>(3); // vertex normals
	layout.Push<float>(2); // vertex texture coords

	va.AddBuffer(vb, layout);
}

/* Model methods definitions */

void Model::Draw(Shader shader)
{
	for (unsigned int i = 0; i < meshes.size(); i++) {
		meshes[i].Draw(shader);
	}
}
