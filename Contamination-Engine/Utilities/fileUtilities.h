#ifndef FILE_UTIL_H
#define FILE_UTIL_H

#include <string>
#include <sstream>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <locale>
#include <codecvt>

namespace CNE {
	namespace Utilities {

		typedef struct Sources_t
		{
			std::string* srcA;
			std::string* srcB;
		};

		std::string LoadFile(std::string path);
		char* LoadFileBytes(std::string path);
		std::string* LoadFileUTF8(std::string path);
		std::wstring StringToWString(const std::string& str);
		std::string WStringToString(const std::wstring& wstr);
	}
}

#endif