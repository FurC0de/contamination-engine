#ifndef RENDERPRIMITIVES_H
#define RENDERPRIMITIVES_H

#include "IndexBuffer.h"
#include "VertexArray.h"

namespace CNE {
	namespace Graphics {
		namespace Rendering {
			class PrimitiveRenderer {
			private:
				unsigned int _id;
			public:
				void Draw(const );

			};
		}
	}
}

#endif