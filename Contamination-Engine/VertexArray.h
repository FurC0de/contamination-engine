#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

namespace CNE {
	namespace Graphics {
		namespace Primitives {
			class VertexArray {
			private:
				unsigned int cne_RendererID;
			public:
				VertexArray();
				~VertexArray();
				void AddBuffer(const VertexBuffer& vb, VertexBufferLayout& layout);
				void Bind() const;
				void Unbind() const;
			};
		}
	}
}


#endif // !VERTEXARRAY_H
