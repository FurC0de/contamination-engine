#ifndef MATERIAL_H
#define MATERIAL_H

#include <glew.h>
#include "../External/glm/glm.hpp"
#include "../External/glm/gtc/matrix_transform.hpp"
#include "../External/glm/gtc/type_ptr.hpp"

#include "../Texture/texture.h"
#include "../Texture/textureManager.h"
#include "../Shaders/shader.h"

using namespace CNE::Graphics::Textures;
using namespace CNE::Graphics::Shaders;

namespace CNE {
	namespace Graphics {
		namespace Materials {
			typedef struct MaterialParams {
				glm::vec3 ambient;
				glm::vec3 diffuse;
				glm::vec3 specular;
				float shininess;
			};

			class Material {
			private:
				unsigned int cne_RendererID;
			
			public:
				Material(ModelTexture* mtextures[7], Shader* shader) : Shader(shader)
				{
					Textures = CTexture::CreateMTCollection(mtextures);

					this->cne_RendererID = 0;
					this->Parameters = {glm::vec3(1), glm::vec3(1), glm::vec3(1), 1.0f};
				};

				/*Material(TextureManager* texture_manager, unsigned int mtextures_hashes[7], Shader* shader) : Shader(shader)
				{

					this->Textures = texture_manager->Find(mtextures_hashes);
					this->cne_RendererID = 0;
					this->Parameters = { glm::vec3(1), glm::vec3(1), glm::vec3(1), 1.0f };
				};
				*/

				Material(TextureManager* texture_manager, unsigned int diffuse_texture_hash, Shader* shader) : Shader(shader)
				{

					this->Textures.Diffuse = texture_manager->Find(diffuse_texture_hash);
					this->cne_RendererID = 0;
					this->Parameters = { glm::vec3(1), glm::vec3(1), glm::vec3(1), 1.0f };
				};

				void Use() const;

				MaterialParams Parameters;
				ModelTextureCollection Textures;
				Shader* Shader;
			};
		}
	}
}

#endif //!MATERIAL_H 