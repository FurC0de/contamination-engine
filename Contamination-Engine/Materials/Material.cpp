#include "Material.h"

using namespace CNE::Graphics::Materials;

void Material::Use() const
{
	Shader->Bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Textures.Diffuse->texID);

	Shader->SetUniform1i("u_Tex", 0);
}
