#ifndef VERTEXBUFFERLAYOUT_H
#define VERTEXBUFFERLAYOUT_H

#ifndef CE_GL_OP
#define CE_GL_OP
/*  Boolean flags definition  */
#define CE_FALSE 0
#define CE_TRUE 1

/*  Byte definition  */
#define CE_BYTE 0x1400
#define CE_UNSIGNED_BYTE 0x1401
#define CE_BYTE_SIZE 1

/*  Short definition  */
#define CE_SHORT 0x1402
#define CE_UNSIGNED_SHORT 0x1403
#define CE_SHORT_SIZE 2

/*  Integer definition  */
#define CE_INT 0x1404
#define CE_UNSIGNED_INT 0x1405
#define CE_INT_SIZE 4

/*  Float definition  */
#define CE_FLOAT 0x1406
#define CE_FLOAT_SIZE 4

/*  Double definition  */
#define CE_DOUBLE 0x140A
#define CE_DOUBLE_SIZE 8

#define CE_FUCK_YOU 0
#endif

#include <vector>

namespace CNE {
	namespace Graphics {
		namespace Primitives {

			typedef struct VertexBufferElement {
				unsigned int type;
				unsigned int count;
				unsigned char isNormalized;

				static unsigned char getElementSize(unsigned short type) {
					switch (type) {
					case CE_BYTE:
					case CE_UNSIGNED_BYTE:
						return CE_BYTE_SIZE;
						break;

					case CE_SHORT:
					case CE_UNSIGNED_SHORT:
						return CE_SHORT_SIZE;
						break;

					case CE_INT:
					case CE_UNSIGNED_INT:
					case CE_FLOAT:
						return CE_FLOAT_SIZE;
						break;

					case CE_DOUBLE:
						return CE_DOUBLE_SIZE;
						break;

					default:
						return CE_FUCK_YOU;
						break;
					}
				}
			};

			class VertexBufferLayout {
			private:
				std::vector<VertexBufferElement> _elements;
				unsigned int _stride;

			public:
				VertexBufferLayout() : _stride(0) {}

				template <typename T>
				void Push(unsigned int count) {
				}

				template<>
				void Push<float>(unsigned int count) {
					_elements.push_back({ CE_FLOAT, count, CE_FALSE });
					_stride += count * CE_FLOAT_SIZE;

				}

				template<>
				void Push<unsigned int>(unsigned int count) {
					_elements.push_back({ CE_UNSIGNED_INT, count, CE_TRUE });
					_stride += count * CE_INT_SIZE;

				}

				template<>
				void Push<unsigned char>(unsigned int count) {
					_elements.push_back({ CE_UNSIGNED_BYTE, count, CE_TRUE });
					_stride += count * CE_BYTE_SIZE;

				}

				inline const std::vector<VertexBufferElement>& GetElements() {
					return _elements;
				}

				inline const unsigned int GetStride() {
					return _stride;
				}

			};
		}
	}
}


#endif // VERTEXBUFFERLAYOUT_H
