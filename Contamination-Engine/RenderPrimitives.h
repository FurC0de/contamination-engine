#ifndef RENDERPRIMITIVES_H
#define RENDERPRIMITIVES_H

#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Materials/Material.h"

using CNE::Graphics::Primitives::VertexArray;
using CNE::Graphics::Primitives::IndexBuffer;
using CNE::Graphics::Materials::Material;

namespace CNE {
	namespace Graphics {
		namespace Rendering {
			class PrimitiveRenderer {
			private:
				unsigned int _id;
			public:
				void Clear();
				void Draw(const VertexArray& va, const IndexBuffer& ib, const Material& mat) const;
			};
		}
	}
}

#endif