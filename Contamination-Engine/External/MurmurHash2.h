#ifndef MMH2
#define MMH2

namespace CNE {
	namespace Utilities {
		unsigned int MurmurHash2(const void* key, int len, unsigned int seed);
	}
}

#endif