#include <glew.h>
#include <glfw3.h>
#include "IndexBuffer.h"

/// data - unsigned int*, count - unsigned int (we shouldn`t use int size multiplication!)
CNE::Graphics::Primitives::IndexBuffer::IndexBuffer(const unsigned int* data, unsigned int count)
	: cne_Count(count)
{
	glGenBuffers(1, &cne_RendererID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cne_RendererID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
}

CNE::Graphics::Primitives::IndexBuffer::~IndexBuffer()
{
	glDeleteBuffers(1, &cne_RendererID);
}

void CNE::Graphics::Primitives::IndexBuffer::Bind() const 
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cne_RendererID);
}

void CNE::Graphics::Primitives::IndexBuffer::Unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
