#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

namespace CNE {
	namespace Graphics {
		namespace Primitives {
			class VertexBuffer {
			private:
				unsigned int cne_RendererID;

			public:
				VertexBuffer(const void* data, unsigned int size);
				~VertexBuffer();
				
				void Bind();
				void Unbind();
			};
		}
	}
}

#endif // !VERTEXBUFFER_H
