#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

namespace CNE {
	namespace Graphics {
		namespace Primitives {
			class IndexBuffer {
			private:
				unsigned int cne_RendererID;
				unsigned int cne_Count;

			public:
				IndexBuffer(const unsigned int* data, unsigned int count);
				~IndexBuffer();

				void Bind() const;
				void Unbind() const;
				
				inline unsigned int GetCount() const { return cne_Count; }
			};
		}
	}
}

#endif // !INDEXBUFFER_H
