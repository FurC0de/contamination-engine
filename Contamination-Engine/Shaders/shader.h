#ifndef SHADER_H
#define SHADER_H

#include <glew.h>
#include <string>
#include <utility>
#include <unordered_map>

#include "../Utilities/fileUtilities.h"


using CNE::Utilities::Sources_t;

namespace CNE {
	namespace Graphics {
		namespace Shaders {

			class Shader
			{
			private:
				unsigned int _id;
				CNE::Utilities::Sources_t _code;
				std::unordered_map<std::string, int> _uLocationCache;


				Sources_t* _readShaderSource(const std::string& vertexPath, const std::string& fragmentPath);
				std::string* _readSingleShaderSource(const std::string& path);
			public:

				Shader(const std::string& vertexPath, const std::string& fragmentPath);
				~Shader();

				unsigned int CompileSingle(const std::string& src, unsigned int type);
				bool Compile();

				void Bind() const;
				void Unbind() const;
				
				int GetUniformLocation(const std::string& name);

				void SetUniform1i(const std::string& name, int value);
			};
		}
	}
}

#endif