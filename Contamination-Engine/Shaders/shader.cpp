#include "shader.h"

using namespace CNE::Graphics::Shaders;

Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath) : _id(0)
{
	_code = *_readShaderSource(vertexPath, fragmentPath);
	Compile();
}

Shader::~Shader()
{
	glDeleteProgram(_id);
}

void Shader::Bind() const
{
	glUseProgram(this->_id);
}

void Shader::Unbind() const
{
	glUseProgram(0);
}

int Shader::GetUniformLocation(const std::string& name)
{
	if (_uLocationCache.find(name) != _uLocationCache.end()) {
		return _uLocationCache[name];
	}

	int location = glGetUniformLocation(_id, name.c_str());
	if (location == -1) {
		std::cout << "Warning: GetUniformLocation unsucceded ( referenced id" << _id << ", not found \"" << name << "\" uniform)" << std::endl;
	} else {
		_uLocationCache.insert({name, location});
	}
	return location;
}

void CNE::Graphics::Shaders::Shader::SetUniform1i(const std::string& name, int value)
{
	glUniform1i(GetUniformLocation(name), value);
}

unsigned int Shader::CompileSingle(const std::string& src, unsigned int type) {
	const char* code = src.c_str();
	unsigned int id = glCreateShader(type);
	glShaderSource(id, 1, &code, NULL);
	glCompileShader(id);
	return id;
}

bool Shader::Compile() {
	int successful;

	/* --------- VERTEX & FRAGMENT --------- */
	unsigned int vertex = Shader::CompileSingle(*(_code.srcA), GL_VERTEX_SHADER);
	unsigned int fragment = Shader::CompileSingle(*(_code.srcB), GL_FRAGMENT_SHADER);

	/* --------- SHADER PROGRAM --------- */
	this->_id = glCreateProgram();
	glAttachShader(this->_id, vertex);
	glAttachShader(this->_id, fragment);
	glLinkProgram(this->_id);

	
	glDeleteShader(vertex);
	glDeleteShader(fragment);

	glGetProgramiv(this->_id, GL_LINK_STATUS, &successful);

	GLchar infoLog[512];
	if (!successful)
	{
		glGetProgramInfoLog(this->_id, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	else {
		std::cout << "SHADER::PROGRAM::LINKING_OK (SHADER "<< this->_id <<" V"<<vertex<<"F"<<fragment<<")" << std::endl;
	}

	return successful;
}

/**/
/**/
/* Internal: */
/**/
/**/

CNE::Utilities::Sources_t* Shader::_readShaderSource(const std::string& vertexPath, const std::string& fragmentPath)
{
	Sources_t* code = new Sources_t;
	code->srcA = new std::string;
	code->srcB = new std::string;

	try
	{
		code->srcA = _readSingleShaderSource(vertexPath);
		code->srcB = _readSingleShaderSource(fragmentPath);
	}
	catch (...)
	{
		std::cout << "ERROR::SHADER::FILES_NOT_SUCCESFULLY_READ" << std::endl;
	}
	
	return code;
}

std::string* Shader::_readSingleShaderSource(const std::string& path)
{
	std::string* source = new std::string("");
	try
	{
		*source = CNE::Utilities::LoadFile(path);
		return source;
	} 
	catch (...) 
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		return source;
	}
}
